devop='on'

if devop == 'on':
    print('CHANGE LOG:\n\nAlpha V2.0:\n-Rewriting the enitre code.\n\nAlpha V2.1:\n-Added scalability to array creation.\n-Added names to difficulty settings.\n-Implemented powerup funcitonality.\n\nAlpha V2.1.1:\n-Squashed some bugs and fixed the space-time continum (allowed players to go on row 0 and 9)\n\nAlpha V2.1.2:\n-Fixed issue where the box chosen would not appear on the display.')

def coordinates_input(player,powerup,colour,distance):
    global gridval
    if player == 1:
        other_player=2
        global p1_powerup
        powerup2=p1_powerup
    else:
        other_player=1
        global p2_powerup
        powerup2=p2_powerup
    if player == 1:
        sq_choice=input('Player 1 choose a square (e.g. 0105): ')
    elif player == 2:
        sq_choice=input('Player 2 choose a square (e.g. 0105): ')
    loop=True
    legal=True
    can_go2=134
    while loop == True:
        if sq_choice == '':
            break
        if legal==False or can_go2==False:
            sq_choice=input('You cannot use that square, please try again: ')
            if sq_choice == '':
                break
        sq_choice=list(sq_choice)
        x_coor=int((sq_choice[0])+(sq_choice[1]))
        y_coor=int((sq_choice[2])+(sq_choice[3]))
        legal=False
        can_go=False
        power_go=False
        if gridval[x_coor][y_coor] == player:
            legal=False
        elif gridval[x_coor][y_coor] == 0:
            legal=True
            can_go=True
        elif gridval[x_coor][y_coor] == other_player:
            legal=False
        global devop
        if devop == 'on':
            print(x_coor)
            print(y_coor)
        can_go2=False

        if can_go == True:
            if x_coor != 0 and x_coor != 9:
                if (gridval[x_coor-1][y_coor] != other_player) and (gridval[x_coor+1][y_coor] != other_player):
                    can_go2=True
                else:
                    can_go2=False
            elif x_coor == 0:
                if gridval[x_coor+1][y_coor] != other_player:
                    can_go2=True
                else:
                    can_go2=False
            elif x_coor == 9:
                if gridval[x_coor-1][y_coor] != other_player:
                    can_go2=True
                else:
                    can_go2=False
            if y_coor != 0 and y_coor != 9:
                if (gridval[x_coor][y_coor-1] != other_player) and (gridval[x_coor][y_coor+1] != other_player):
                    hghhhjh=0
                else:
                    can_go2=False
            elif y_coor == 0:
                if gridval[x_coor][y_coor+1] != other_player:
                    hghhhjh=0
                else:
                    can_go2=False
            elif y_coor == 9:
                if gridval[x_coor][y_coor-1] != other_player:
                    hghhhjh=0
                else:
                    can_go2=False
        

        if can_go2 == True:
            gridval[x_coor][y_coor] = player
            loop=False
            drawing_sqaures(x_coor,y_coor,distance,player,colour)
            powerup2=powerup2+1
        elif can_go2 == False and legal != False:
            if powerup >= 3:
                choice2=input('Would you like to use a powerup (y/n): ')
                if choice2 == 'y':
                    gridval[x_coor][y_coor] = player
                    drawing_sqaures(x_coor,y_coor,distance,player,colour)
                    powerup2=powerup2-3
                    powerup2=powerup2+1
    print(gridval)
    return sq_choice
            

def drawing_sqaures (x_value,y_value,distance,player,colour):
    timmy.tracer(0)
    timmy.pencolor("black")
    timmy.penup()
    timmy.goto((-160+(distance*x_value)),(160-(distance*y_value)))
    timmy.pendown()
    timmy.fillcolor(colour)
    timmy.begin_fill()
    for i in range (0,4):
        timmy.forward(distance)
        timmy.right(90)
    timmy.end_fill()
    timmy.update()
    

def grid_creation (Difficulty):
    val=Difficulty*10
    arr1=[] 
    for i in range (0,val):
        arr1.append(0)
    arr2=[]
    for i in range (0,val):
        arr2.append(arr1)
    print(arr2)
    return arr2

        



# written by James Baker
import turtle
import math
import random
from random import randint

T=turtle
timmy=turtle
T.speed(100)
T.penup()
T.goto(-160,272)

def Score(X,Y,Z):
    T.penup()
    T.color(X)
    T.goto(-160+(320/((Dif)*10))*Y,160-(320/(Dif*10))*Z)
    if Y>=1 and Y<=Dif*10 and Z>=1 and Z<=Dif*10:
        T.begin_fill()
        for i in range(0,4):
            T.forward(320/(Dif*10))
            T.lt(90)
        T.end_fill()
    
def Box(X,Y,Z):
    T.color(X)
    T.goto(Y,Z)
    T.begin_fill()
    for i in range(0,4):
        T.forward(8)
        T.lt(90)
    T.end_fill()

def ForLoop():
    Tru=Tru+1
    return Tru

def Sect(Col,Amount,x,y):
    if devop != 'on':
        T.tracer(0)
    if Amount==1:
        Box(Col,-160+8*x,272-8*y)
    else:
        for i in range (0,Amount):
            Box(Col,-160+8*(x+i),272-8*y)
    if devop != 'on':
        T.update()

if devop != 'on':
    T.hideturtle()

choice=input('To start a new game, type "n". To load an old game, type "o": ')

if choice == 'n':

    #The difficulty and grid setup system

    print('\n\nPlease choose a difficulty:\n\n1: The Developer\n2: Blip Lite\n3: Blipa Romana\n4: Blipo Classico\n5: The Scientist\n6: The Satanist\n7: Unlucky for One\n8: 8 Bit Blip\n9: The Square\n10: The Madematician\n')            
    Dif=int(input("Difficulty Level (1-10): "))
    if devop=='on':
        turtle.tracer(0)
    T.goto(-160,-160)
    T.color("Grey")
    T.pendown()
    for a in range (0,Dif*10+2):
        T.forward (320)
        T.penup()
        T.goto(-160,-160+320/(Dif*10)*a)
        T.pendown()
    T.rt(90)
    T.penup()
    for a in range (0,Dif*10+2):
        T.forward (320)
        T.penup()
        T.goto(-160+320/(Dif*10)*a,160)
        T.pendown()
    T.penup()
    T.goto(0,0)
    T.goto(-160,272)
    colors  = ["red","green","blue","orange","purple","pink","yellow"]
    Col1="Black"
    Col2=random.choice(colors)
    colors.remove(Col2)
    Col3=random.choice(colors)
    colors.remove(Col3)

    #The logo for the game in the startup screen

    Num=0
    Sect(Col1,8,0,Num)#B
    Sect(Col1,5,12,Num)#L
    Sect(Col1,4,24,Num)#I
    Sect(Col1,8,30,Num)#P
    Num=1
    Sect(Col1,1,0,Num)#B
    Sect(Col2,7,1,Num)
    Sect(Col1,1,8,Num)
    Sect(Col1,1,12,Num)#L
    Sect(Col2,3,13,Num)
    Sect(Col1,1,16,Num)
    Sect(Col1,1,24,Num)#I
    Sect(Col2,2,25,Num)
    Sect(Col1,1,27,Num)
    Sect(Col1,1,30,Num)#P
    Sect(Col2,7,31,Num)
    Sect(Col1,1,38,Num)
    Num=2
    Sect(Col1,1,0,Num)#B
    Sect(Col2,2,1,Num)
    Sect(Col1,4,3,Num)
    Sect(Col2,2,7,Num)
    Sect(Col1,1,9,Num)
    Sect(Col1,1,12,Num)#L
    Sect(Col2,3,13,Num)
    Sect(Col1,1,16,Num)
    Sect(Col1,1,24,Num)#I
    Sect(Col2,2,25,Num)
    Sect(Col1,1,27,Num)
    Sect(Col1,1,30,Num)#P
    Sect(Col2,2,31,Num)
    Sect(Col1,4,33,Num)
    Sect(Col2,2,37,Num)
    Sect(Col1,1,39,Num)
    Num=3
    Sect(Col1,1,0,Num)#B
    Sect(Col2,2,1,Num)
    Sect(Col1,1,3,Num)
    Sect(Col1,1,7,Num)
    Sect(Col2,2,8,Num)
    Sect(Col1,1,9,Num)
    Sect(Col1,1,12,Num)#L
    Sect(Col2,3,13,Num)
    Sect(Col1,1,16,Num)
    Sect(Col1,1,24,Num)#I
    Sect(Col2,2,25,Num)
    Sect(Col1,1,27,Num)
    Sect(Col1,1,30,Num)#P
    Sect(Col2,2,31,Num)
    Sect(Col1,1,33,Num)
    Sect(Col1,1,37,Num)
    Sect(Col2,1,38,Num)
    Sect(Col1,1,39,Num)
    Num=4
    Sect(Col1,1,0,Num)#B
    Sect(Col2,2,1,Num)
    Sect(Col1,4,3,Num)
    Sect(Col2,2,7,Num)
    Sect(Col1,1,9,Num)
    Sect(Col1,1,12,Num)#L
    Sect(Col2,3,13,Num)
    Sect(Col1,1,16,Num)
    Sect(Col1,1,24,Num)#I
    Sect(Col2,2,25,Num)
    Sect(Col1,1,27,Num)
    Sect(Col1,1,30,Num)#P
    Sect(Col2,2,31,Num)
    Sect(Col1,1,33,Num)
    Sect(Col1,1,37,Num)
    Sect(Col2,1,38,Num)
    Sect(Col1,1,39,Num)
    Num=5
    Sect(Col1,1,0,Num)#B
    Sect(Col2,7,1,Num)
    Sect(Col1,1,8,Num)
    Sect(Col1,1,12,Num)#L
    Sect(Col2,3,13,Num)
    Sect(Col1,1,16,Num)
    Sect(Col1,1,24,Num)#I
    Sect(Col2,2,25,Num)
    Sect(Col1,1,27,Num)
    Sect(Col1,1,30,Num)#P
    Sect(Col2,2,31,Num)
    Sect(Col1,1,33,Num)
    Sect(Col1,1,37,Num)
    Sect(Col2,1,38,Num)
    Sect(Col1,1,39,Num)
    Num=6
    Sect(Col1,1,0,Num)#B
    Sect(Col3,2,1,Num)
    Sect(Col1,4,3,Num)
    Sect(Col3,2,7,Num)
    Sect(Col1,1,9,Num)
    Sect(Col1,1,12,Num)#L
    Sect(Col3,3,13,Num)
    Sect(Col1,1,16,Num)
    Sect(Col1,1,24,Num)#I
    Sect(Col3,2,25,Num)
    Sect(Col1,1,27,Num)
    Sect(Col1,1,30,Num)#P
    Sect(Col3,2,31,Num)
    Sect(Col1,4,33,Num)
    Sect(Col3,2,37,Num)
    Sect(Col1,1,39,Num)
    Num=7
    Sect(Col1,1,0,Num)#B
    Sect(Col3,2,1,Num)
    Sect(Col1,1,3,Num)
    Sect(Col1,1,7,Num)
    Sect(Col3,2,8,Num)
    Sect(Col1,1,9,Num)
    Sect(Col1,1,12,Num)#L
    Sect(Col3,3,13,Num)
    Sect(Col1,1,16,Num)
    Sect(Col1,1,24,Num)#I
    Sect(Col3,2,25,Num)
    Sect(Col1,1,27,Num)
    Sect(Col1,1,30,Num)#P
    Sect(Col3,7,31,Num)
    Sect(Col1,1,38,Num)
    Num=8
    Sect(Col1,1,0,Num)#B
    Sect(Col3,2,1,Num)
    Sect(Col1,1,3,Num)
    Sect(Col1,1,7,Num)
    Sect(Col3,2,8,Num)
    Sect(Col1,1,9,Num)
    Sect(Col1,1,12,Num)#L
    Sect(Col3,3,13,Num)
    Sect(Col1,1,16,Num)
    Sect(Col1,3,19,Num)
    Sect(Col1,1,24,Num)#I
    Sect(Col3,2,25,Num)
    Sect(Col1,1,27,Num)
    Sect(Col1,1,30,Num)#P
    Sect(Col3,2,31,Num)
    Sect(Col1,5,33,Num)
    Num=9
    Sect(Col1,1,0,Num)#B
    Sect(Col3,2,1,Num)
    Sect(Col1,4,3,Num)
    Sect(Col3,2,7,Num)
    Sect(Col1,1,9,Num)
    Sect(Col1,1,12,Num)#L
    Sect(Col3,3,13,Num)
    Sect(Col1,4,16,Num)
    Sect(Col3,1,20,Num)
    Sect(Col1,1,21,Num)
    Sect(Col1,1,24,Num)#I
    Sect(Col3,2,25,Num)
    Sect(Col1,1,27,Num)
    Sect(Col1,1,30,Num)#P
    Sect(Col3,2,31,Num)
    Sect(Col1,1,33,Num)
    Num=10
    Sect(Col1,1,0,Num)#B
    Sect(Col3,7,1,Num)
    Sect(Col1,1,8,Num)
    Sect(Col1,1,12,Num)#L
    Sect(Col3,8,13,Num)
    Sect(Col1,1,21,Num)
    Sect(Col1,1,24,Num)#I
    Sect(Col3,2,25,Num)
    Sect(Col1,1,27,Num)
    Sect(Col1,1,30,Num)#P
    Sect(Col3,2,31,Num)
    Sect(Col1,1,33,Num)
    Num=11
    Sect(Col1,8,0,Num)#B
    Sect(Col1,10,12,Num)#L
    Sect(Col1,4,24,Num)#I
    Sect(Col1,4,30,Num)#P


    # Written by Jack Smith
    
    
    # timy goto the top posoition
    timmy.goto(-160,160)
    timmy.left(90)
    if devop=='on':
        turtle.update()

    powerup=3
    gridval=grid_creation(Dif)
    dist=320/(Dif*10)

    p1pu=0
    p2pu=0
    choice1='s'

    range1=10*Dif
    
    timmy.goto(-160,-170)
    for i in range (0,range1):
        xval3=(-160+(dist*i)+(dist/2))
        timmy.goto(xval3,-170)
        if len(list(str(i))) == 1:
            ist=str(i)
            newi='0'+ist
        else:
            newi=i
        timmy.write(newi)

    
    timmy.goto(-170,-160)
    for i in range (0,range1):
        yval3=(-160+(dist*i)+(dist/2))
        timmy.goto(-170,yval3)
        if len(list(str(i))) == 1:
            i5=(10*Dif-(i+1))
            ist=str(i5)
            newi='0'+ist
        else:
            newi=i
        timmy.write(newi)

    run = True
    p1_powerup=0
    p2_powerup=0


    choice3='a'
    while choice3 != '':
        choice3=coordinates_input(1,p1_powerup,Col2,dist)
        choice3=coordinates_input(2,p2_powerup,Col3,dist)
        
