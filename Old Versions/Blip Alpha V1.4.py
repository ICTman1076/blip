# written by James Baker
import turtle
import math
import random
from random import randint

T=turtle
timmy=turtle
T.speed(100)
T.penup()
T.goto(-160,272)

def Score(X,Y,Z):
    T.penup()
    T.color(X)
    T.goto(-160+(320/((Dif)*10))*Y,160-(320/(Dif*10))*Z)
    if Y>=1 and Y<=Dif*10 and Z>=1 and Z<=Dif*10:
        T.begin_fill()
        for i in range(0,4):
            T.forward(320/(Dif*10))
            T.lt(90)
        T.end_fill()
    
def Box(X,Y,Z):
    T.color(X)
    T.goto(Y,Z)
    T.begin_fill()
    for i in range(0,4):
        T.forward(8)
        T.lt(90)
    T.end_fill()

def ForLoop():
    Tru=Tru+1
    return Tru

def Sect(Col,Amount,x,y):
    if Amount==1:
        Box(Col,-160+8*x,272-8*y)
    else:
        for i in range (0,Amount):
            Box(Col,-160+8*(x+i),272-8*y)

choice=input('To start a new game, type "n". To load an old game, type "o": ')

if choice == 'n':

    #The difficulty and grid setup system
                
    Dif=int(input("Difficulty Level (1-4): "))
    turtle.tracer(0)
    T.goto(-160,-160)
    T.color("Grey")
    T.pendown()
    for a in range (0,Dif*10+2):
        T.forward (320)
        T.penup()
        T.goto(-160,-160+320/(Dif*10)*a)
        T.pendown()
    T.rt(90)
    T.penup()
    for a in range (0,Dif*10+2):
        T.forward (320)
        T.penup()
        T.goto(-160+320/(Dif*10)*a,160)
        T.pendown()
    T.penup()
    T.goto(0,0)
    T.goto(-160,272)
    colors  = ["red","green","blue","orange","purple","pink","yellow"]
    Col1="Black"
    Col2=random.choice(colors)
    Col3=random.choice(colors)

    #The logo for the game in the startup screen

    Num=0
    Sect(Col1,8,0,Num)#B
    Sect(Col1,5,12,Num)#L
    Sect(Col1,4,24,Num)#I
    Sect(Col1,8,30,Num)#P
    Num=1
    Sect(Col1,1,0,Num)#B
    Sect(Col2,7,1,Num)
    Sect(Col1,1,8,Num)
    Sect(Col1,1,12,Num)#L
    Sect(Col2,3,13,Num)
    Sect(Col1,1,16,Num)
    Sect(Col1,1,24,Num)#I
    Sect(Col2,2,25,Num)
    Sect(Col1,1,27,Num)
    Sect(Col1,1,30,Num)#P
    Sect(Col2,7,31,Num)
    Sect(Col1,1,38,Num)
    Num=2
    Sect(Col1,1,0,Num)#B
    Sect(Col2,2,1,Num)
    Sect(Col1,4,3,Num)
    Sect(Col2,2,7,Num)
    Sect(Col1,1,9,Num)
    Sect(Col1,1,12,Num)#L
    Sect(Col2,3,13,Num)
    Sect(Col1,1,16,Num)
    Sect(Col1,1,24,Num)#I
    Sect(Col2,2,25,Num)
    Sect(Col1,1,27,Num)
    Sect(Col1,1,30,Num)#P
    Sect(Col2,2,31,Num)
    Sect(Col1,4,33,Num)
    Sect(Col2,2,37,Num)
    Sect(Col1,1,39,Num)
    Num=3
    Sect(Col1,1,0,Num)#B
    Sect(Col2,2,1,Num)
    Sect(Col1,1,3,Num)
    Sect(Col1,1,7,Num)
    Sect(Col2,2,8,Num)
    Sect(Col1,1,9,Num)
    Sect(Col1,1,12,Num)#L
    Sect(Col2,3,13,Num)
    Sect(Col1,1,16,Num)
    Sect(Col1,1,24,Num)#I
    Sect(Col2,2,25,Num)
    Sect(Col1,1,27,Num)
    Sect(Col1,1,30,Num)#P
    Sect(Col2,2,31,Num)
    Sect(Col1,1,33,Num)
    Sect(Col1,1,37,Num)
    Sect(Col2,1,38,Num)
    Sect(Col1,1,39,Num)
    Num=4
    Sect(Col1,1,0,Num)#B
    Sect(Col2,2,1,Num)
    Sect(Col1,4,3,Num)
    Sect(Col2,2,7,Num)
    Sect(Col1,1,9,Num)
    Sect(Col1,1,12,Num)#L
    Sect(Col2,3,13,Num)
    Sect(Col1,1,16,Num)
    Sect(Col1,1,24,Num)#I
    Sect(Col2,2,25,Num)
    Sect(Col1,1,27,Num)
    Sect(Col1,1,30,Num)#P
    Sect(Col2,2,31,Num)
    Sect(Col1,1,33,Num)
    Sect(Col1,1,37,Num)
    Sect(Col2,1,38,Num)
    Sect(Col1,1,39,Num)
    Num=5
    Sect(Col1,1,0,Num)#B
    Sect(Col2,7,1,Num)
    Sect(Col1,1,8,Num)
    Sect(Col1,1,12,Num)#L
    Sect(Col2,3,13,Num)
    Sect(Col1,1,16,Num)
    Sect(Col1,1,24,Num)#I
    Sect(Col2,2,25,Num)
    Sect(Col1,1,27,Num)
    Sect(Col1,1,30,Num)#P
    Sect(Col2,2,31,Num)
    Sect(Col1,1,33,Num)
    Sect(Col1,1,37,Num)
    Sect(Col2,1,38,Num)
    Sect(Col1,1,39,Num)
    Num=6
    Sect(Col1,1,0,Num)#B
    Sect(Col3,2,1,Num)
    Sect(Col1,4,3,Num)
    Sect(Col3,2,7,Num)
    Sect(Col1,1,9,Num)
    Sect(Col1,1,12,Num)#L
    Sect(Col3,3,13,Num)
    Sect(Col1,1,16,Num)
    Sect(Col1,1,24,Num)#I
    Sect(Col3,2,25,Num)
    Sect(Col1,1,27,Num)
    Sect(Col1,1,30,Num)#P
    Sect(Col3,2,31,Num)
    Sect(Col1,4,33,Num)
    Sect(Col3,2,37,Num)
    Sect(Col1,1,39,Num)
    Num=7
    Sect(Col1,1,0,Num)#B
    Sect(Col3,2,1,Num)
    Sect(Col1,1,3,Num)
    Sect(Col1,1,7,Num)
    Sect(Col3,2,8,Num)
    Sect(Col1,1,9,Num)
    Sect(Col1,1,12,Num)#L
    Sect(Col3,3,13,Num)
    Sect(Col1,1,16,Num)
    Sect(Col1,1,24,Num)#I
    Sect(Col3,2,25,Num)
    Sect(Col1,1,27,Num)
    Sect(Col1,1,30,Num)#P
    Sect(Col3,7,31,Num)
    Sect(Col1,1,38,Num)
    Num=8
    Sect(Col1,1,0,Num)#B
    Sect(Col3,2,1,Num)
    Sect(Col1,1,3,Num)
    Sect(Col1,1,7,Num)
    Sect(Col3,2,8,Num)
    Sect(Col1,1,9,Num)
    Sect(Col1,1,12,Num)#L
    Sect(Col3,3,13,Num)
    Sect(Col1,1,16,Num)
    Sect(Col1,3,19,Num)
    Sect(Col1,1,24,Num)#I
    Sect(Col3,2,25,Num)
    Sect(Col1,1,27,Num)
    Sect(Col1,1,30,Num)#P
    Sect(Col3,2,31,Num)
    Sect(Col1,5,33,Num)
    Num=9
    Sect(Col1,1,0,Num)#B
    Sect(Col3,2,1,Num)
    Sect(Col1,4,3,Num)
    Sect(Col3,2,7,Num)
    Sect(Col1,1,9,Num)
    Sect(Col1,1,12,Num)#L
    Sect(Col3,3,13,Num)
    Sect(Col1,4,16,Num)
    Sect(Col3,1,20,Num)
    Sect(Col1,1,21,Num)
    Sect(Col1,1,24,Num)#I
    Sect(Col3,2,25,Num)
    Sect(Col1,1,27,Num)
    Sect(Col1,1,30,Num)#P
    Sect(Col3,2,31,Num)
    Sect(Col1,1,33,Num)
    Num=10
    Sect(Col1,1,0,Num)#B
    Sect(Col3,7,1,Num)
    Sect(Col1,1,8,Num)
    Sect(Col1,1,12,Num)#L
    Sect(Col3,8,13,Num)
    Sect(Col1,1,21,Num)
    Sect(Col1,1,24,Num)#I
    Sect(Col3,2,25,Num)
    Sect(Col1,1,27,Num)
    Sect(Col1,1,30,Num)#P
    Sect(Col3,2,31,Num)
    Sect(Col1,1,33,Num)
    Num=11
    Sect(Col1,8,0,Num)#B
    Sect(Col1,10,12,Num)#L
    Sect(Col1,4,24,Num)#I
    Sect(Col1,4,30,Num)#P


    # Written by Jack Smith
    
    
    # timy goto the top posoition
    timmy.goto(-160,160)
    timmy.left(90)

    turtle.update()

    gridref10=['0000', '0100', '0200', '0300', '0400', '0500', '0600', '0700', '0800', '0900', '0001', '0101', '0201', '0301', '0401', '0501', '0601', '0701', '0801', '0901', '0002', '0102', '0202', '0302', '0402', '0502', '0602', '0702', '0802', '0902', '0003', '0103', '0203', '0303', '0403', '0503', '0603', '0703', '0803', '0903', '0004', '0104', '0204', '0304', '0404', '0504', '0604', '0704', '0804', '0904', '0005', '0105', '0205', '0305', '0405', '0505', '0605', '0705', '0805', '0905', '0006', '0106', '0206', '0306', '0406', '0506', '0606', '0706', '0806', '0906', '0007', '0107', '0207', '0307', '0407', '0507', '0607', '0707', '0807', '0907', '0008', '0108', '0208', '0308', '0408', '0508', '0608', '0708', '0808', '0908', '0009', '0109', '0209', '0309', '0409', '0509', '0609', '0709', '0809', '0909']

    if Dif == 1:
        dist=32
        gridref=gridref10
        gridval=[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    p1pu=0
    p2pu=0
    choice1='s'
    
    # player 1 go 1
    playera = 1
    abc=input('Please enter the Y value for your first go (if one number, please input as 01, for eg: ')
    if playera == 1:
        valuea = '00'
    elif playera == 2:
        valuea = '09'
    choicea=valuea+abc
    abci=int(abc)
    len3=((len(gridref))-1)
    for i in range (0,len3):
        if choicea == gridref[i]:
            value=i
            print('FOUND')
    gridrefval=gridval[value]
    cy=(dist*(int(abci)))
    if playera == 1:
        finalx=-160
    elif playera == 2:
        finalx=(160-dist)
    finaly=160-cy
    turtle.tracer(0)
    timmy.speed(100)
    timmy.goto(finalx,finaly)
    if playera == 1:
        colorpl=Col2
    elif playera == 2:
        colorpl=Col3
    timmy.color("Grey")
    timmy.fillcolor(colorpl)
    timmy.pendown()
    timmy.begin_fill()
    for i in range (0,4):
        timmy.forward(32)
        timmy.right(90)
    timmy.end_fill()
    timmy.penup()
    timmy.update()
    print(len(gridval))
    gridval.pop(value)
    gridval.insert(value,playera)
    print(gridval)
    print(len(gridval))
    if playera == 1:
        p1pu=p1pu+1
        print(p1pu)
    elif playera == 2:
        p2pu=powu+1
        print(p2pu)

    #player 2 go 1
    playera = 2
    abc=input('Please enter the Y value for your first go (if one number, please input as 01, for eg: ')
    if playera == 1:
        valuea = '00'
    elif playera == 2:
        valuea = '09'
    print('valuea',valuea)
    print('abc',abc)
    choicea=valuea+abc
    print('choicea',choicea)
    abci=int(abc)
    len3=((len(gridref)))
    for i in range (0,len3):
        if choicea == gridref[i]:
            value=i
            print('FOUND')
    print('value',value)
    gridrefval=gridval[value]
    cy=(dist*(int(abci)))
    if playera == 1:
        finalx=-160
    elif playera == 2:
        finalx=(160-dist)
    finaly=160-cy
    turtle.tracer(0)
    timmy.speed(100)
    timmy.goto(finalx,finaly)
    timmy.color("Grey")
    if playera == 1:
        colorpl=Col2
    elif playera == 2:
        colorpl=Col3
    timmy.fillcolor(colorpl)
    timmy.pendown()
    timmy.begin_fill()
    for i in range (0,4):
        timmy.forward(32)
        timmy.right(90)
    timmy.end_fill()
    timmy.penup()
    timmy.update()
    print(len(gridval))
    gridval.pop(value)
    gridval.insert(value,playera)
    print(gridval)
    print(len(gridval))
    if playera == 1:
        p1pu=p1pu+1
        print(p1pu)
    elif playera == 2:
        p2pu=p2pu+1
        print(p2pu)
        
    while choice1 != 'f':
        print('choice1',choice1)
        #player 1
        opppl=2
        player=1
        colorpl=Col2
        go2=False
        if player == 1:
            powu=p1pu
        elif player == 2:
            powu=p2pu
        while go2==False:
            choice1=input('Player 1 please choose a square: ')
            len3=((len(gridref)))
            for i in range (0,len3):
                if choice1 == gridref[i]:
                    value=i
            gridrefval=gridval[value]

            if gridrefval == 0:
                good=True
            else:
                good=False

            if good == False:
                choice1=input('That square is already taken, please choose another: ')
                len3=((len(gridref))-1)
                for i in range (0,len3):
                    if choice1 == gridref[i]:
                        value=i
                gridrefval=gridval[value]
                if gridrefval == 0:
                    good=True
                else:
                    good=False

            chosesq=gridref[value]
            chosesq2 =list(chosesq)
            ch1=str(chosesq2[0])
            ch2=str(chosesq2[1])
            final1=ch1+ch2
            ch3=str(chosesq2[2])
            ch4=str(chosesq2[3])
            final2=ch3+ch4
            finali1=int(final1)
            finali2=int(final2)
            if finali1 != 0:
                newv1=finali1-1
                go=True
            elif finali1 != 0:
                newv1=finali1-1
                go=True
            else:
                go=False
                c1=1
            if go == True:
                f1=str(newv1)
                if (len(f1)) == 1:
                    f1='0'+f1
                f2=str(finali2)
                if (len(f2)) == 1:
                    f2='0'+f2
                f3=f1+f2
                for i in range (0,((len(gridref))-1)):
                    if f3 == gridref[i]:
                        var3=i
                if gridval[var3] == 0:
                    c1=1
                elif gridval[var3] == opppl:
                    c1=0
                else:
                    c1=1
                    
            if finali2 != 0:
                newv2=finali2-1
                go=True
            elif finali2 != 0:
                newv2=finali2-1
                go=True
            else:
                go=False
                c2=1

            if go == True:
                f1=str(finali1)
                if (len(f1)) == 1:
                    f1='0'+f1
                f2=str(newv2)
                if (len(f2)) == 1:
                    f2='0'+f2
                f3=f1+f2
                for i in range (0,((len(gridref))-1)):
                    if f3 == gridref[i]:
                        var3=i
                if gridval[var3] == 0:
                    c2=1
                elif gridval[var3] == opppl:
                    c2=0
                else:
                    c2=1
                    
            if finali1 != 9:
                newv1=finali1+1
                go=True
            else:
                go=False
                c3=1
            if go == True:
                f1=str(newv1)
                if (len(f1)) == 1:
                    f1='0'+f1
                f2=str(finali2)
                if (len(f2)) == 1:
                    f2='0'+f2
                f3=f1+f2
                for i in range (0,((len(gridref))-1)):
                    if f3 == gridref[i]:
                        var3=i
                if gridval[var3] == 0:
                    c3=1
                elif gridval[var3] == opppl:
                    c3=0
                else:
                    c3=1

            if finali2 != 9:
                print(finali2)
                newv2=finali2+1
                go=True
                print('true')
            else:
                go=False
                print('false')
                c4=1
            if go == True:
                f1=str(finali1)
                print('f1',f1)
                if (len(f1)) == 1:
                    f1='0'+f1
                    print('new f1',f1)
                f2=str(newv2)
                if (len(f2)) == 1:
                    f2='0'+f2
                    print('new f2',f2)
                f3=f1+f2
                print('f3',f3)
                for i in range (0,((len(gridref))-1)):
                    if f3 == gridref[i]:
                        var3=i
                print('var3',var3)
                print('gridval var3',gridval[var3])
                print('gridval var3-1',gridval[var3-1])
                print('gridval var3+1',gridval[var3+1])
                if gridval[var3] == 0:
                    c4=1
                elif gridval[var3] == opppl:
                    c4=0
                else:
                    c4=1
                if f3 == '0909':
                    c4=0

            if c1+c2+c3+c4 == 4:
                go2=True
            else:
                go2=False

            if go2==False:
                if powu >= 5:
                    pu=input('Would you like to use a power up? (y/n): ')
                    if pu == 'y':
                        go2=True
                        if player == 1:
                            p1pu=p1pu-5
                        elif player == 2:
                            p2pu=powu-5
                    elif pu == 'n':
                        go2=False

            if go2 == True:
                cx=(dist*(int(final1)))
                cy=(dist*(int(final2)))
                finalx=-160+cx
                finaly=160-cy
                turtle.tracer(0)
                timmy.speed(100)
                timmy.goto(finalx,finaly)
                timmy.color("Grey")
                timmy.fillcolor(colorpl)
                timmy.pendown()
                timmy.begin_fill()
                for i in range (0,4):
                    timmy.forward(32)
                    timmy.right(90)
                timmy.end_fill()
                timmy.penup()
                timmy.update()
                print(len(gridval))
                gridval.pop(value)
                gridval.insert(value,player)
                print(gridval)
                print(len(gridval))
                if player == 1:
                    p1pu=p1pu+1
                    print(p1pu)
                elif player == 2:
                    p2pu=p2pu+1
                    print(p2pu)
                    



        #player 2
        opppl=1
        player=2
        colorpl=Col3
        go2=False
        if player == 1:
            powu=p1pu
        elif player == 2:
            powu=p2pu
        while go2 == False:
            choice1=input('Player 2 please choose a square: ')
            len3=((len(gridref)))
            for i in range (0,len3):
                if choice1 == gridref[i]:
                    value=i
            gridrefval=gridval[value]

            if gridrefval == 0:
                good=True
            else:
                good=False

            if good == False:
                choice1=input('That square is already taken, please choose another: ')
                len3=((len(gridref))-1)
                for i in range (0,len3):
                    if choice1 == gridref[i]:
                        value=i
                gridrefval=gridval[value]
                if gridrefval == 0:
                    good=True
                else:
                    good=False

            chosesq=gridref[value]
            chosesq2 =list(chosesq)
            ch1=str(chosesq2[0])
            ch2=str(chosesq2[1])
            final1=ch1+ch2
            ch3=str(chosesq2[2])
            ch4=str(chosesq2[3])
            final2=ch3+ch4
            finali1=int(final1)
            finali2=int(final2)
            if finali1 != 0:
                newv1=finali1-1
                go=True
            elif finali1 != 0:
                newv1=finali1-1
                go=True
            else:
                go=False
                c1=1
            if go == True:
                f1=str(newv1)
                if (len(f1)) == 1:
                    f1='0'+f1
                f2=str(finali2)
                if (len(f2)) == 1:
                    f2='0'+f2
                f3=f1+f2
                for i in range (0,((len(gridref))-1)):
                    if f3 == gridref[i]:
                        var3=i
                if gridval[var3] == 0:
                    c1=1
                elif gridval[var3] == opppl:
                    c1=0
                else:
                    c1=1
                    
            if finali2 != 0:
                newv2=finali2-1
                go=True
            elif finali2 != 0:
                newv2=finali2-1
                go=True
            else:
                go=False
                c2=1

            if go == True:
                f1=str(finali1)
                if (len(f1)) == 1:
                    f1='0'+f1
                f2=str(newv2)
                if (len(f2)) == 1:
                    f2='0'+f2
                f3=f1+f2
                for i in range (0,((len(gridref))-1)):
                    if f3 == gridref[i]:
                        var3=i
                if gridval[var3] == 0:
                    c2=1
                elif gridval[var3] == opppl:
                    c2=0
                else:
                    c2=1
                    
            if finali1 != 9:
                newv1=finali1+1
                go=True
            else:
                go=False
                c3=1
            if go == True:
                f1=str(newv1)
                if (len(f1)) == 1:
                    f1='0'+f1
                f2=str(finali2)
                if (len(f2)) == 1:
                    f2='0'+f2
                f3=f1+f2
                for i in range (0,((len(gridref))-1)):
                    if f3 == gridref[i]:
                        var3=i
                if gridval[var3] == 0:
                    c3=1
                elif gridval[var3] == opppl:
                    c3=0
                else:
                    c3=1

            if finali2 != 0:
                newv2=finali2-1
                go=True
            else:
                go=False
                c4=1
            if go == True:
                f1=str(finali1)
                if (len(f1)) == 1:
                    f1='0'+f1
                f2=str(newv2)
                if (len(f2)) == 1:
                    f2='0'+f2
                f3=f1+f2
                for i in range (0,((len(gridref))-1)):
                    if f3 == gridref[i]:
                        var3=i
                if gridval[var3] == 0:
                    c4=1
                elif gridval[var3] == opppl:
                    c4=0
                else:
                    c4=1
                if f3 == '0909':
                    c4=0

            if c1+c2+c3+c4 == 4:
                go2=True
            else:
                go2=False

            if go2==False:
                if powu >= 5:
                    pu=input('Would you like to use a power up? (y/n): ')
                    if pu == 'y':
                        go2=True
                        if player == 1:
                            p1pu=p1pu-5
                        elif player == 2:
                            p2pu=powu-5
                    elif pu == 'n':
                        go2=False

            if go2 == True:
                cx=(dist*(int(final1)))
                cy=(dist*(int(final2)))
                finalx=-160+cx
                finaly=160-cy
                turtle.tracer(0)
                timmy.speed(100)
                timmy.goto(finalx,finaly)
                timmy.color("Grey")
                timmy.fillcolor(colorpl)
                timmy.pendown()
                timmy.begin_fill()
                for i in range (0,4):
                    timmy.forward(32)
                    timmy.right(90)
                timmy.end_fill()
                timmy.penup()
                timmy.update()
                print(len(gridval))
                gridval.pop(value)
                gridval.insert(value,player)
                print(gridval)
                print(len(gridval))
                if player == 1:
                    p1pu=p1pu+1
                    print(p1pu)
                elif player == 2:
                    p2pu=p2pu+1
                    print(p2pu)


    choice5=input('Would you like to save (s) or discard (d) the game: ')
    if choice5 == 's':
        finals=gridval
        finals.insert(0,p2pu)
        finals.insert(0,p1pu)
        finals.insert(0,Col3)
        finals.insert(0,Col2)
        finals.insert(0,Dif)
        fname=input('Please enter a file name: ')
        file1=open(fname+'.txt','w')
        for i in range (0,((len(finals)))):
            file1.write(str(finals[i]))
        file1.close()
        

elif choice == 'o':
    #open old file
    print('hi')

print('Thank you for playing Blip.\n\nIdea and graphics made by James Baker. Restof the coding done by Jack Smith.')


        
    
    
            


























