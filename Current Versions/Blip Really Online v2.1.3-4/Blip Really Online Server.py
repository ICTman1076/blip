from bottle import route, run, get
from copy import deepcopy

gamesData = {}

@get('/submit/<name>/<player>/<pos>')
def submit(name,player,pos):
	global gamesData
	gamesData[name][player] = pos
	return "True"

@get("/getPos/<name>/<player>")
def getPos(name, player):
	global gamesData
	toRet = deepcopy(gamesData[name][player])
	gamesData[name][player] = "waiting"
	return toRet

@get("/newGame/<name>")
def newGame(name):
	global gamesData
	gamesData[name] = {
		"p1": "waiting",
		"p2": "waiting"
	}
	return name

run(host='0.0.0.0', port=80)