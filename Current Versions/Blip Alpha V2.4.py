devop='off'

if devop == 'on':
    print('CHANGE LOG:\n\nAlpha V2.0:\n-Rewriting the enitre code.\n\nAlpha V2.1:\n-Added scalability to array creation.\n-Added names to difficulty settings.\n-Implemented powerup funcitonality.\n\nAlpha V2.1.1:\n-Squashed some bugs and fixed the space-time continum (allowed players to go on row 0 and 9)\n\nAlpha V2.1.2:\n-Fixed issue where the box chosen would not appear on the display.\n\nAlpha V2.1.4:\n-Fixed an issue where powerups would not function properly.\n\nAlpha V2.2\n-Added theme functionality from files.\n\nAplha V2.3:\n-Started to implement the winning algorithm section of the program.\n\nAlpha V2.3.1:\n-Fixed an issue where the winning algorithm wouldnt run after using a powerup.\n\nAlpha V2.4:\n-Added a built in Hague Edition.\n-Removed the choice function as it was not used.\n-Added a graphic to show powerup levels.\n-Added a graphic to show best places to go.')

can_x=[]
can_y=[]


def winning_algorithm (player):
    global gridval
    global Dif
    won=False
    x_points=[]
    y_points=[]
    up=[]
    down=[]
    left=[]
    right=[]
    go=False
    for i in range (0,(len(gridval[0]))):
        if gridval[0][i] == player:
            x_points.append(0)
            y_points.append(i)
            left.append(False)
            if i != 0:
                if gridval[0][i-1] == player:
                    up.append(True)
                else:
                    up.append(False)
            else:
                up.append(False)
            if i != ((Dif*10)-1):
                if gridval[0][i+1] == player:
                    down.append(True)
                else:
                    down.append(False)
            else:
                down.append(False)
            if gridval[1][i] == player:
                right.append(True)
            else:
                right.append(False)
            if up[0] == False and down[0] == False and right[0] == False:
                go=False
            else:
                go=True
    if go == True:
        l=(len(up)-1)
        a=True
        b=0
        while l>= 0 and b<=1000:
            b=b+1
            if x_points[l] == ((Dif*10)-1):
                won=True
                break
            a=True
            if up[l] == True and a == True:
                x=x_points[(l)]
                y=y_points[(l)]
                y1=y-1
                x_points.append(x)
                y_points.append(y1)
                if y!= 0:
                    if gridval[x][y-1] == player:
                        up.append(True)
                    else:
                        up.append(False)
                else:
                    up.append(False)
                down.append(False)
                if x != ((Dif*10)-1):
                    if gridval[x+1][y] == player:
                        right.append(True)
                    else:
                        right.append(False)
                else: 
                    right.append(False)
                if x != 0:
                    if gridval[x-1][y] == player:
                        left.append(True)
                    else:
                        left.append(False)
                else:
                    left.append(False)
                l=l+1
                a=False
                if left[l] == False and right[l] == False and up[l] == False:
                    x_points.pop(l)
                    y_points.pop(l)
                    up.pop(l)
                    down.pop(l)
                    left.pop(l)
                    right.pop(l)
                    up[l-1] = False
                    a=False
                    l=l-1
            if right[(l)] == True  and a == True:
                x=x_points[(l)]
                y=y_points[(l)]
                x1=x+1
                x_points.append(x1)
                y_points.append(y)
                if y!= 0:
                    if gridval[x][y-1] == player:
                        up.append(True)
                    else:
                        up.append(False)
                else:
                    up.append(False)
                if y!= ((Dif*10)-1):
                    if gridval[x][y+1] == player:
                        down.append(True)
                    else:
                        down.append(False)
                else:
                    down.append(False)
                left.append(False)
                if x != ((Dif*10)-1):
                    if gridval[x+1][y] == player:
                        right.append(True)
                    else:
                        right.append(False)
                else:
                    right.append(False)
                l=l+1
                a=False
                if right[l] == False and up[l] == False and down[l] == False:
                    x_points.pop(l)
                    y_points.pop(l)
                    up.pop(l)
                    down.pop(l)
                    left.pop(l)
                    right.pop(l)
                    right[l-1] = False
                    a=False
                    l=l-1

            if down[(l)] == True and a == True:
                x=x_points[(l)]
                y=y_points[(l)]
                y1=y+1
                x_points.append(x)
                y_points.append(y1)
                if y!= ((Dif*10)-1):
                    if gridval[x][y+1] == player:
                        down.append(True)
                    else:
                        down.append(False)
                else:
                    down.append(False)
                up.append(False)
                if x != ((Dif*10)-1):
                    if gridval[x+1][y] == player:
                        right.append(True)
                    else:
                        right.append(False)
                else: 
                    right.append(False)
                if x != 0:
                    if gridval[x-1][y] == player:
                        left.append(True)
                    else:
                        left.append(False)
                else:
                    left.append(False)
                l=l+1
                a=False
                if left[l] == False and right[l] == False and down[l] == False:
                    x_points.pop(l)
                    y_points.pop(l)
                    up.pop(l)
                    down.pop(l)
                    left.pop(l)
                    right.pop(l)
                    down[l-1] = False
                    a=False
                    l=l-1

            if left[(l)] == True and a == True:
                x=x_points[(l)]
                y=y_points[(l)]
                x1=x-1
                x_points.append(x1)
                y_points.append(y)
                if y!= 0:
                    if gridval[x][y-1] == player:
                        up.append(True)
                    else:
                        up.append(False)
                else:
                    up.append(False)
                if y!= ((Dif*10)-1):
                    if gridval[x][y+1] == player:
                        down.append(True)
                    else:
                        down.append(False)
                else:
                    down.append(False)
                right.append(False)
                if x != ((Dif*10)-1):
                    if gridval[x+1][y] == player:
                        left.append(True)
                    else:
                        left.append(False)
                else:
                    left.append(False)
                l=l+1
                a=False
                if left[l] == False and up[l] == False and down[l] == False:
                    x_points.pop(l)
                    y_points.pop(l)
                    up.pop(l)
                    down.pop(l)
                    left.pop(l)
                    right.pop(l)
                    left[l-1] = False
                    a=False
                    l=l-1

    return won


def powerup_create (player,colour):
    timmy.penup()
    if player == 1:
        xs=200
        ys=150
        timmy.color(colour)
        timmy.goto((xs+25),(ys+10))
        timmy.write('Player 1')
    elif player == 2:
        xs=200
        ys=-50
        timmy.goto((xs+25),(ys+10))
        timmy.write('Player 2')
    xs2=xs
    timmy.tracer(0)
    for i in range (0,9):
        Box(colour,xs2,ys)
        xs2=xs2+8
    timmy.update()
    xs3=xs
    ys3=ys
    for i in range (0,5):
        timmy.tracer(0)
        for i in range (0,2):
            ys3=ys3-8
            Box(colour,xs3,ys3)
        timmy.update()
        xs3=xs3+8
    xs4=xs2-8
    ys4=ys
    for i in range (0,4):
        timmy.tracer(0)
        for i in range (0,2):
            ys4=ys4-8
            Box(colour,xs4,ys4)
        timmy.update()
        xs4=xs4-8

def powerup_draw (player,colour,value):
    global bg
    timmy.penup()
    if player == 1:
        xs=208
        ys=142
    elif player == 2:
        xs=208
        ys=-58
    if value == 0:
        ys2=ys
        j=7
        xa=xs
        for k in range (0,10):
            for l in range (0,2):
                xs2=xa
                for i in range(0,j):
                    Box(bg,xs2,ys2)
                    xs2=xs2+8 
                ys2=ys2-8
            xa=xa+8
            j=j-2
    elif value == 1:
        xsa=xs+24
        ysa=ys-48
        Box(colour,xsa,ysa)
        Box(colour,xsa,(ysa-8))
    elif value == 2:
        xsa=xs+16
        ysa=ys-32
        for j in range (0,2):
            for i in range (0,3):
                Box(colour,(xsa+(i*8)),ysa)
            ysa=ysa-8
    elif value == 3:
        xsa=xs+8
        ysa=ys-16
        for j in range (0,2):
            for i in range (0,5):
                Box(colour,(xsa+(i*8)),ysa)
            ysa=ysa-8
    elif value == 4:
        xsa=xs
        ysa=ys
        for j in range (0,2):
            for i in range (0,7):
                Box(colour,(xsa+(i*8)),ysa)
            ysa=ysa-8


def possible_go (player,colour,xl,yl,distance):
    global can_x
    global can_y
    can_x=[]
    can_y=[]
    global gridval
    for i in range (0,(len(xl))):
        x=xl[i]
        y=yl[i]
        if x!= 0:
            x1=x-1
            y1=y
            if gridval[x1][y1] == 0:
                can_x.append(x1)
                can_y.append(y1)
        if x!=9:
            x1=x+1
            y1=y
            if gridval[x1][y1] == 0:
                can_x.append(x1)
                can_y.append(y1)
        if y!=9:
            x1=x
            y1=y+1
            if gridval[x1][y1] == 0:
                can_x.append(x1)
                can_y.append(y1)
        if y!=0:
            x1=x
            y1=y-1
            if gridval[x1][y1] == 0:
                can_x.append(x1)
                can_y.append(y1)
    for i in range (0,len(can_y)):
        drawing_sqaures((can_x[i]),(can_y[i]),distance,player,'#B1FF96')

def remove_pg (distance):
    global bg
    global can_y
    global can_x
    for i in range (0,len(can_x)):
        drawing_sqaures((can_x[i]),(can_y[i]),distance,1,bg)

def coordinates_input(player,powerup,colour,distance):
    global last_x1
    global last_y2
    global last_x2
    global last_y1
    global has_won
    global gridval
    if player == 1:
        other_player=2
        global p1_powerup
    else:
        other_player=1
        global p2_powerup
    if (len(last_x1)) != 0 and (len(last_x2)) != 0:
        if player == 1:
            possible_go(player,'green',last_x1,last_y1,distance)
        elif player == 2:
            possible_go(player,'green',last_x2,last_y2,distance)
    if player == 1:
        sq_choice=input('Player 1 choose a square (e.g. 0105): ')
    elif player == 2:
        sq_choice=input('Player 2 choose a square (e.g. 0105): ')
    if (len(last_x1)) != 0 and (len(last_x2)) != 0:
        remove_pg(distance)
    loop=True
    legal=True
    can_go2=134
    while loop == True:
        if sq_choice == '':
            break
        if legal==False or can_go2==False:
            sq_choice=input('You cannot use that square, please try again: ')
            if sq_choice == '':
                break
        sq_choice=list(sq_choice)
        x_coor=int((sq_choice[0])+(sq_choice[1]))
        y_coor=int((sq_choice[2])+(sq_choice[3]))
        legal=False
        can_go=False
        if gridval[x_coor][y_coor] == player:
            legal=False
        elif gridval[x_coor][y_coor] == 0:
            legal=True
            can_go=True
        elif gridval[x_coor][y_coor] == other_player:
            legal=False
        global devop
        if devop == 'on':
            print(x_coor)
            print(y_coor)
        can_go2=False

        if can_go == True:
            if x_coor != 0 and x_coor != 9:
                if (gridval[x_coor-1][y_coor] != other_player) and (gridval[x_coor+1][y_coor] != other_player):
                    can_go2=True
                else:
                    can_go2=False
            elif x_coor == 0:
                if gridval[x_coor+1][y_coor] != other_player:
                    can_go2=True
                else:
                    can_go2=False
            elif x_coor == 9:
                if gridval[x_coor-1][y_coor] != other_player:
                    can_go2=True
                else:
                    can_go2=False
            if y_coor != 0 and y_coor != 9:
                if (gridval[x_coor][y_coor-1] != other_player) and (gridval[x_coor][y_coor+1] != other_player):
                    hghhhjh=0
                else:
                    can_go2=False
            elif y_coor == 0:
                if gridval[x_coor][y_coor+1] != other_player:
                    hghhhjh=0
                else:
                    can_go2=False
            elif y_coor == 9:
                if gridval[x_coor][y_coor-1] != other_player:
                    hghhhjh=0
                else:
                    can_go2=False
        

        if can_go2 == True:
            gridval[x_coor][y_coor] = player
            loop=False
            drawing_sqaures(x_coor,y_coor,distance,player,colour)
            has_won=winning_algorithm(player)
            if has_won == True:
                has_won=player
            if player == 1:
                p1_powerup=p1_powerup+1
                powerup_draw(1,colour,p1_powerup)
                last_x1.append(x_coor)
                last_y1.append(y_coor)
            else:
                p2_powerup=p2_powerup+1
                powerup_draw(2,colour,p2_powerup)
                last_x2.append(x_coor)
                last_y2.append(y_coor)
        elif can_go2 == False and legal != False:
            if powerup >= 4:
                choice2=input('Would you like to use a powerup (y/n): ')
                if choice2 == 'y':
                    gridval[x_coor][y_coor] = player
                    drawing_sqaures(x_coor,y_coor,distance,player,colour)
                    has_won=winning_algorithm(player)
                    if has_won == True:
                        has_won=player
                    if player == 1:
                        last_x1.append(x_coor)
                        last_y1.append(y_coor)
                        p1_powerup=p1_powerup-4
                        powerup_draw(1,colour,p1_powerup)
                    else:
                        p2_powerup=p2_powerup-4
                        powerup_draw(2,colour,p2_powerup)
                        last_x2.append(x_coor)
                        last_y2.append(y_coor)
                    loop=False
    return sq_choice
            

def drawing_sqaures (x_value,y_value,distance,player,colour):
    timmy.tracer(0)
    timmy.pencolor("black")
    timmy.penup()
    timmy.goto((-160+(distance*x_value)),(160-(distance*y_value)))
    timmy.pendown()
    timmy.fillcolor(colour)
    timmy.begin_fill()
    for i in range (0,4):
        timmy.forward(distance)
        timmy.right(90)
    timmy.end_fill()
    timmy.update()
    

def grid_creation (Difficulty):
    val=Difficulty*10
    arr1=[[0 for i in range (val)] for i in range (val)] 
    return arr1
    
def FileCheck (file_name):
    import os.path
    go=True
    # validation for the file name
    while go == True:
        if os.path.isfile(file_name+'.txt'):
            #Checks if file exists
            go=False
        else:
            file_name=input('File not found, please try again: ')
            # If it doesnt exist, the user is asked to try again
    return file_name # Returns filename to program
        



# written by James Baker
import turtle
import math
import random
from random import randint

T=turtle
timmy=turtle
T.speed(100)
T.penup()
T.goto(-160,272)

def Score(X,Y,Z):
    T.penup()
    T.color(X)
    T.goto(-160+(320/((Dif)*10))*Y,160-(320/(Dif*10))*Z)
    if Y>=1 and Y<=Dif*10 and Z>=1 and Z<=Dif*10:
        T.begin_fill()
        for i in range(0,4):
            T.forward(320/(Dif*10))
            T.lt(90)
        T.end_fill()
    
def Box(X,Y,Z):
    T.color(X)
    T.goto(Y,Z)
    T.begin_fill()
    for i in range(0,4):
        T.forward(8)
        T.lt(90)
    T.end_fill()

def ForLoop():
    Tru=Tru+1
    return Tru

def Sect(Col,Amount,x,y):
    if devop != 'on':
        T.tracer(0)
    if Amount==1:
        Box(Col,-160+8*x,272-8*y)
    else:
        for i in range (0,Amount):
            Box(Col,-160+8*(x+i),272-8*y)
    if devop != 'on':
        T.update()

if devop != 'on':
    T.hideturtle()

des_choice=input('Would you like to open the Blip theme from a file (f), hague edition (h) or use the standard (s) theme: ')
if des_choice == 'f':
    file_name=input('Please choose the filename: ')
    file_name=FileCheck(file_name)
    file1=open(file_name+'.txt','r')
    info=file1.readlines()
    for i in range (0,(len(info))):
        temp=list(info[i])
        t2=''
        if temp[(len(temp)-1)] == '\n':
            temp.pop(len(temp)-1)
        for j in range (0,len(temp)):
            t2=t2+temp[j]
        info[i]=t2
    design=info
    acc=design[3]
    timmy.bgcolor(design[0])
    bg=design[0]
elif des_choice == 'h':
    timmy.bgcolor('#FF0000')
    acc='#FFFFFF'
    bg='#FF0000'
else:
    acc='black'
    bg='white'
    
#choice=input('To start a new game, type "n". To load an old game, type "o": ')
choice='n'
if choice == 'n':

    #The difficulty and grid setup system

    print('\n\nPlease choose a difficulty:\n\n1: The Developer\n2: Blip Lite\n3: Blipa Romana\n4: Blipo Classico\n5: The Scientist\n6: The Satanist\n7: Unlucky for One\n8: 8 Bit Blip\n9: The Square\n10: The Madematician\n')            
    Dif=int(input("Difficulty Level (1-10): "))
    if devop=='on':
        turtle.tracer(0)
    T.goto(-160,-160)
    if des_choice == 'f':
        T.color(design[3])
    elif des_choice == 'h':
        T.color('#FFFFFF')
    else:
        T.color("Grey")
    T.pendown()
    for a in range (0,Dif*10+2):
        T.forward (320)
        T.penup()
        T.goto(-160,-160+320/(Dif*10)*a)
        T.pendown()
    T.rt(90)
    T.penup()
    for a in range (0,Dif*10+2):
        T.forward (320)
        T.penup()
        T.goto(-160+320/(Dif*10)*a,160)
        T.pendown()
    T.penup()
    T.goto(0,0)
    T.goto(-160,272)
    colors  = ["red","green","blue","orange","purple","pink","yellow"]
    Col1="Black"
    Col2=random.choice(colors)
    colors.remove(Col2)
    Col3=random.choice(colors)
    colors.remove(Col3)

    if des_choice == 'f':
        Col2=design[1]
        Col3=design[2]
    elif des_choice == 'h':
        Col2='#FFFFFF'
        Col3='#9D9D9D'
    #The logo for the game in the startup screen

    Num=0
    Sect(Col1,8,0,Num)#B
    Sect(Col1,5,12,Num)#L
    Sect(Col1,4,24,Num)#I
    Sect(Col1,8,30,Num)#P
    Num=1
    Sect(Col1,1,0,Num)#B
    Sect(Col2,7,1,Num)
    Sect(Col1,1,8,Num)
    Sect(Col1,1,12,Num)#L
    Sect(Col2,3,13,Num)
    Sect(Col1,1,16,Num)
    Sect(Col1,1,24,Num)#I
    Sect(Col2,2,25,Num)
    Sect(Col1,1,27,Num)
    Sect(Col1,1,30,Num)#P
    Sect(Col2,7,31,Num)
    Sect(Col1,1,38,Num)
    Num=2
    Sect(Col1,1,0,Num)#B
    Sect(Col2,2,1,Num)
    Sect(Col1,4,3,Num)
    Sect(Col2,2,7,Num)
    Sect(Col1,1,9,Num)
    Sect(Col1,1,12,Num)#L
    Sect(Col2,3,13,Num)
    Sect(Col1,1,16,Num)
    Sect(Col1,1,24,Num)#I
    Sect(Col2,2,25,Num)
    Sect(Col1,1,27,Num)
    Sect(Col1,1,30,Num)#P
    Sect(Col2,2,31,Num)
    Sect(Col1,4,33,Num)
    Sect(Col2,2,37,Num)
    Sect(Col1,1,39,Num)
    Num=3
    Sect(Col1,1,0,Num)#B
    Sect(Col2,2,1,Num)
    Sect(Col1,1,3,Num)
    Sect(Col1,1,7,Num)
    Sect(Col2,2,8,Num)
    Sect(Col1,1,9,Num)
    Sect(Col1,1,12,Num)#L
    Sect(Col2,3,13,Num)
    Sect(Col1,1,16,Num)
    Sect(Col1,1,24,Num)#I
    Sect(Col2,2,25,Num)
    Sect(Col1,1,27,Num)
    Sect(Col1,1,30,Num)#P
    Sect(Col2,2,31,Num)
    Sect(Col1,1,33,Num)
    Sect(Col1,1,37,Num)
    Sect(Col2,1,38,Num)
    Sect(Col1,1,39,Num)
    Num=4
    Sect(Col1,1,0,Num)#B
    Sect(Col2,2,1,Num)
    Sect(Col1,4,3,Num)
    Sect(Col2,2,7,Num)
    Sect(Col1,1,9,Num)
    Sect(Col1,1,12,Num)#L
    Sect(Col2,3,13,Num)
    Sect(Col1,1,16,Num)
    Sect(Col1,1,24,Num)#I
    Sect(Col2,2,25,Num)
    Sect(Col1,1,27,Num)
    Sect(Col1,1,30,Num)#P
    Sect(Col2,2,31,Num)
    Sect(Col1,1,33,Num)
    Sect(Col1,1,37,Num)
    Sect(Col2,1,38,Num)
    Sect(Col1,1,39,Num)
    Num=5
    Sect(Col1,1,0,Num)#B
    Sect(Col2,7,1,Num)
    Sect(Col1,1,8,Num)
    Sect(Col1,1,12,Num)#L
    Sect(Col2,3,13,Num)
    Sect(Col1,1,16,Num)
    Sect(Col1,1,24,Num)#I
    Sect(Col2,2,25,Num)
    Sect(Col1,1,27,Num)
    Sect(Col1,1,30,Num)#P
    Sect(Col2,2,31,Num)
    Sect(Col1,1,33,Num)
    Sect(Col1,1,37,Num)
    Sect(Col2,1,38,Num)
    Sect(Col1,1,39,Num)
    Num=6
    Sect(Col1,1,0,Num)#B
    Sect(Col3,2,1,Num)
    Sect(Col1,4,3,Num)
    Sect(Col3,2,7,Num)
    Sect(Col1,1,9,Num)
    Sect(Col1,1,12,Num)#L
    Sect(Col3,3,13,Num)
    Sect(Col1,1,16,Num)
    Sect(Col1,1,24,Num)#I
    Sect(Col3,2,25,Num)
    Sect(Col1,1,27,Num)
    Sect(Col1,1,30,Num)#P
    Sect(Col3,2,31,Num)
    Sect(Col1,4,33,Num)
    Sect(Col3,2,37,Num)
    Sect(Col1,1,39,Num)
    Num=7
    Sect(Col1,1,0,Num)#B
    Sect(Col3,2,1,Num)
    Sect(Col1,1,3,Num)
    Sect(Col1,1,7,Num)
    Sect(Col3,2,8,Num)
    Sect(Col1,1,9,Num)
    Sect(Col1,1,12,Num)#L
    Sect(Col3,3,13,Num)
    Sect(Col1,1,16,Num)
    Sect(Col1,1,24,Num)#I
    Sect(Col3,2,25,Num)
    Sect(Col1,1,27,Num)
    Sect(Col1,1,30,Num)#P
    Sect(Col3,7,31,Num)
    Sect(Col1,1,38,Num)
    Num=8
    Sect(Col1,1,0,Num)#B
    Sect(Col3,2,1,Num)
    Sect(Col1,1,3,Num)
    Sect(Col1,1,7,Num)
    Sect(Col3,2,8,Num)
    Sect(Col1,1,9,Num)
    Sect(Col1,1,12,Num)#L
    Sect(Col3,3,13,Num)
    Sect(Col1,1,16,Num)
    Sect(Col1,3,19,Num)
    Sect(Col1,1,24,Num)#I
    Sect(Col3,2,25,Num)
    Sect(Col1,1,27,Num)
    Sect(Col1,1,30,Num)#P
    Sect(Col3,2,31,Num)
    Sect(Col1,5,33,Num)
    Num=9
    Sect(Col1,1,0,Num)#B
    Sect(Col3,2,1,Num)
    Sect(Col1,4,3,Num)
    Sect(Col3,2,7,Num)
    Sect(Col1,1,9,Num)
    Sect(Col1,1,12,Num)#L
    Sect(Col3,3,13,Num)
    Sect(Col1,4,16,Num)
    Sect(Col3,1,20,Num)
    Sect(Col1,1,21,Num)
    Sect(Col1,1,24,Num)#I
    Sect(Col3,2,25,Num)
    Sect(Col1,1,27,Num)
    Sect(Col1,1,30,Num)#P
    Sect(Col3,2,31,Num)
    Sect(Col1,1,33,Num)
    Num=10
    Sect(Col1,1,0,Num)#B
    Sect(Col3,7,1,Num)
    Sect(Col1,1,8,Num)
    Sect(Col1,1,12,Num)#L
    Sect(Col3,8,13,Num)
    Sect(Col1,1,21,Num)
    Sect(Col1,1,24,Num)#I
    Sect(Col3,2,25,Num)
    Sect(Col1,1,27,Num)
    Sect(Col1,1,30,Num)#P
    Sect(Col3,2,31,Num)
    Sect(Col1,1,33,Num)
    Num=11
    Sect(Col1,8,0,Num)#B
    Sect(Col1,10,12,Num)#L
    Sect(Col1,4,24,Num)#I
    Sect(Col1,4,30,Num)#P


    # Written by Jack Smith
    
    
    # timy goto the top posoition
    timmy.goto(-160,160)
    timmy.left(90)
    if devop=='on':
        turtle.update()

    powerup=3
    gridval=grid_creation(Dif)
    dist=320/(Dif*10)

    p1pu=0
    p2pu=0
    choice1='s'

    range1=10*Dif
    
    timmy.goto(-160,-170)
    for i in range (0,range1):
        xval3=(-160+(dist*i)+(dist/2))
        timmy.goto(xval3,-170)
        if len(list(str(i))) == 1:
            ist=str(i)
            newi='0'+ist
        else:
            newi=i
        if des_choice == 'f':
            timmy.color(design[3])
        elif des_choice == 'h':
            timmy.color('#FFFFFF')
        timmy.write(newi)

    
    timmy.goto(-170,-160)
    for i in range (0,range1):
        yval3=(-160+(dist*i)+(dist/2))
        timmy.goto(-170,yval3)
        if len(list(str(i))) == 1:
            i5=(10*Dif-(i+1))
            ist=str(i5)
            newi='0'+ist
        else:
            newi=i
        if des_choice == 'f':
            timmy.color(design[3])
        elif des_choice == 'h':
            timmy.color('#FFFFFF')
        timmy.write(newi)

    powerup_create(1,acc)
    powerup_create(2,acc)
    last_x1=[]
    last_y1=[]
    last_x2=[]
    last_y2=[]

    run = True
    p1_powerup=0
    p2_powerup=0

    has_won=False
    choice3='a'
    while choice3 != '':
        choice3=coordinates_input(1,p1_powerup,Col2,dist)
        if has_won == 1:
            print('Player 1 has won the game')
            break
        choice3=coordinates_input(2,p2_powerup,Col3,dist)
        if has_won == 2:
            print('Player 2 has won the game')
            break
        
