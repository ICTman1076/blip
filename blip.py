import time
devop='off'
#yeet
if devop == 'on':
    print('CHANGE LOG:\n\nAlpha V2.0:\n-Rewriting the enitre code.\n\nAlpha V2.1:\n-Added scalability to array creation.\n-Added names to difficulty settings.\n-Implemented powerup funcitonality.\n\nAlpha V2.1.1:\n-Squashed some bugs and fixed the space-time continum (allowed players to go on row 0 and 9)\n\nAlpha V2.1.2:\n-Fixed issue where the box chosen would not appear on the display.')

def send_mail (coordinates):
    c2=''
    for i in range (0,len(coordinates)):
        c2=c2+coordinates[i]
    import time
    import smtplib
    from email.mime.text import MIMEText
    from email.mime.multipart import MIMEMultipart
    global email_user
    global password
    global sendto
    global emailcount 
    print('Sending email, please wait a moment...')
    subject = 'Blip '+(str(emailcount))
    emailcount=emailcount+1
    server = smtplib.SMTP ('smtp.gmail.com',587)
    server.starttls ()
    server.login (email_user,password)
    msg = MIMEMultipart ()
    msg ['From'] = email_user
    msg ['To'] = sendto
    msg ['Subject'] = subject
    body = str(c2)
    msg.attach (MIMEText (body,'plain') )
    text = msg.as_string ()
    server.sendmail (email_user,sendto,text )
    print('Email sent')

def recieve_mail ():
    sub_true='a'
    c=0
    while sub_true != True:
        print('Recieving email, please wait...'+str(c))
        c=c+1
        time.sleep(1)
        import email
        import imaplib
        global email_user
        EMAIL = email_user
        if EMAIL.split("@")[1] == "gmail.com":
            SERVER = 'imap.gmail.com'
        else:
            SERVER = input("Enter IMAP server: ")
        global password
        PASSWORD = password
        

        # connect to the server and go to its inbox
        mail = imaplib.IMAP4_SSL(SERVER)
        mail.login(EMAIL, PASSWORD)
        # we choose the inbox but you can select others
        mail.select('inbox')

        # we'll search using the ALL criteria to retrieve
        # every message inside the inbox
        # it will return with its status and a list of ids
        status, data = mail.search(None, 'ALL')
        # the list returned is a list of bytes separated
        # by white spaces on this format: [b'1 2 3', b'4 5 6']
        # so, to separate it first we create an empty list
        mail_ids = []
        # then we go through the list splitting its blocks
        # of bytes and appending to the mail_ids list
        for block in data:
            # the split function called without parameter
            # transforms the text or bytes into a list using
            # as separator the white spaces:
            # b'1 2 3'.split() => [b'1', b'2', b'3']
            mail_ids += block.split()

        # now for every id we'll fetch the email
        # to extract its content
        for i in mail_ids:
            # the fetch function fetch the email given its id
            # and format that you want the message to be
            status, data = mail.fetch(i, '(RFC822)')

            # the content data at the '(RFC822)' format comes on
            # a list with a tuple with header, content, and the closing
            # byte b')'
            for response_part in data:
                # so if its a tuple...
                if isinstance(response_part, tuple):
                    # we go for the content at its second element
                    # skipping the header at the first and the closing
                    # at the third
                    message = email.message_from_bytes(response_part[1])

                    # with the content we can extract the info about
                    # who sent the message and its subject
                    mail_from = message['from']
                    mail_subject = message['subject']

                    # then for the text we have a little more work to do
                    # because it can be in plain text or multipart
                    # if its not plain text we need to separate the message
                    # from its annexes to get the text
                    if message.is_multipart():
                        mail_content = ''

                        # on multipart we have the text message and
                        # another things like annex, and html version
                        # of the message, in that case we loop through
                        # the email payload
                        for part in message.get_payload():
                            # if the content type is text/plain
                            # we extract it
                            if part.get_content_type() == 'text/plain':
                                mail_content += part.get_payload()
                    else:
                        # if the message isn't multipart, just extract it
                        mail_content = message.get_payload()

                    # and then let's show its result
                    global emailcount
                    if mail_subject == ('Blip '+str(emailcount)):
                        emailcount=emailcount+1
                        mc2=list(mail_content)
                        x_coordinates=int((mc2[0])+(mc2[1]))
                        y_coordinates=int((mc2[2])+(mc2[3]))
                        global gridval
                        global player_number
                        global dist
                        global oplayer_colour
                        sub_true=True
                        print('Email recieved')
                        if player_number == 1:
                            pn2=2
                        elif player_number == 2:
                            pn2=1
                        gridval[x_coordinates][y_coordinates] = pn2
                        drawing_sqaures(x_coordinates,y_coordinates,dist,pn2,oplayer_colour)
                        dhvdh=1
                        break

            

def coordinates_input(player,powerup,colour,distance):
    global gridval
    if player == 1:
        other_player=2
        global p1_powerup
        powerup2=p1_powerup
    else:
        other_player=1
        global p2_powerup
        powerup2=p2_powerup
    if player == 1:
        sq_choice=input('Player 1 choose a square (e.g. 0105): ')
    elif player == 2:
        sq_choice=input('Player 2 choose a square (e.g. 0105): ')
    loop=True
    legal=True
    can_go2=134
    while loop == True:
        if sq_choice == '':
            break
        if legal==False or can_go2==False:
            sq_choice=input('You cannot use that square, please try again: ')
            if sq_choice == '':
                break
        sq_choice=list(sq_choice)
        x_coor=int((sq_choice[0])+(sq_choice[1]))
        y_coor=int((sq_choice[2])+(sq_choice[3]))
        legal=False
        can_go=False
        power_go=False
        if gridval[x_coor][y_coor] == player:
            legal=False
        elif gridval[x_coor][y_coor] == 0:
            legal=True
            can_go=True
        elif gridval[x_coor][y_coor] == other_player:
            legal=False
        global devop
        if devop == 'on':
            print(x_coor)
            print(y_coor)
        can_go2=False

        if can_go == True:
            if x_coor != 0 and x_coor != 9:
                if (gridval[x_coor-1][y_coor] != other_player) and (gridval[x_coor+1][y_coor] != other_player):
                    can_go2=True
                else:
                    can_go2=False
            elif x_coor == 0:
                if gridval[x_coor+1][y_coor] != other_player:
                    can_go2=True
                else:
                    can_go2=False
            elif x_coor == 9:
                if gridval[x_coor-1][y_coor] != other_player:
                    can_go2=True
                else:
                    can_go2=False
            if y_coor != 0 and y_coor != 9:
                if (gridval[x_coor][y_coor-1] != other_player) and (gridval[x_coor][y_coor+1] != other_player):
                    hghhhjh=0
                else:
                    can_go2=False
            elif y_coor == 0:
                if gridval[x_coor][y_coor+1] != other_player:
                    hghhhjh=0
                else:
                    can_go2=False
            elif y_coor == 9:
                if gridval[x_coor][y_coor-1] != other_player:
                    hghhhjh=0
                else:
                    can_go2=False
        

        if can_go2 == True:
            gridval[x_coor][y_coor] = player
            loop=False
            drawing_sqaures(x_coor,y_coor,distance,player,colour)
            powerup2=powerup2+1
        elif can_go2 == False and legal != False:
            if powerup >= 3:
                choice2=input('Would you like to use a powerup (y/n): ')
                if choice2 == 'y':
                    gridval[x_coor][y_coor] = player
                    drawing_sqaures(x_coor,y_coor,distance,player,colour)
                    powerup2=powerup2-3
                    powerup2=powerup2+1
    send_mail(sq_choice)
    return sq_choice
            

def drawing_sqaures (x_value,y_value,distance,player,colour):
    timmy.tracer(0)
    timmy.pencolor("black")
    timmy.penup()
    timmy.goto((-160+(distance*x_value)),(160-(distance*y_value)))
    timmy.pendown()
    timmy.fillcolor(colour)
    timmy.begin_fill()
    for i in range (0,4):
        timmy.forward(distance)
        timmy.right(90)
    timmy.end_fill()
    timmy.update()
    

def grid_creation (Difficulty):
    val=Difficulty*10
    arr1=[[0 for i in range (val)] for i in range (val)] 
    return arr1
    

        



# written by James Baker
import turtle
import math
import random
from random import randint

T=turtle
timmy=turtle
T.speed(100)
T.penup()
T.goto(-160,272)

def Score(X,Y,Z):
    T.penup()
    T.color(X)
    T.goto(-160+(320/((Dif)*10))*Y,160-(320/(Dif*10))*Z)
    if Y>=1 and Y<=Dif*10 and Z>=1 and Z<=Dif*10:
        T.begin_fill()
        for i in range(0,4):
            T.forward(320/(Dif*10))
            T.lt(90)
        T.end_fill()
    
def Box(X,Y,Z):
    T.color(X)
    T.goto(Y,Z)
    T.begin_fill()
    for i in range(0,4):
        T.forward(8)
        T.lt(90)
    T.end_fill()

def ForLoop():
    Tru=Tru+1
    return Tru

def Sect(Col,Amount,x,y):
    if devop != 'on':
        T.tracer(0)
    if Amount==1:
        Box(Col,-160+8*x,272-8*y)
    else:
        for i in range (0,Amount):
            Box(Col,-160+8*(x+i),272-8*y)
    if devop != 'on':
        T.update()

if devop != 'on':
    T.hideturtle()

choice=input('To start a new game, type "n". To load an old game, type "o": ')

if choice == 'n':

    #The difficulty and grid setup system

    print('\n\nPlease choose a difficulty:\n\n1: The Developer\n2: Blip Lite\n3: Blipa Romana\n4: Blipo Classico\n5: The Scientist\n6: The Satanist\n7: Unlucky for One\n8: 8 Bit Blip\n9: The Square\n10: The Madematician\n')            
    Dif=int(input("Difficulty Level (1-10): "))
    if devop=='on':
        turtle.tracer(0)
    T.goto(-160,-160)
    T.color("Grey")
    T.pendown()
    for a in range (0,Dif*10+2):
        T.forward (320)
        T.penup()
        T.goto(-160,-160+320/(Dif*10)*a)
        T.pendown()
    T.rt(90)
    T.penup()
    for a in range (0,Dif*10+2):
        T.forward (320)
        T.penup()
        T.goto(-160+320/(Dif*10)*a,160)
        T.pendown()
    T.penup()
    T.goto(0,0)
    T.goto(-160,272)
    colors  = ["red","green","blue","orange","purple","pink","yellow"]
    Col1="Black"
    Col2=random.choice(colors)
    colors.remove(Col2)
    Col3=random.choice(colors)
    colors.remove(Col3)

    #The logo for the game in the startup screen

    Num=0
    Sect(Col1,8,0,Num)#B
    Sect(Col1,5,12,Num)#L
    Sect(Col1,4,24,Num)#I
    Sect(Col1,8,30,Num)#P
    Num=1
    Sect(Col1,1,0,Num)#B
    Sect(Col2,7,1,Num)
    Sect(Col1,1,8,Num)
    Sect(Col1,1,12,Num)#L
    Sect(Col2,3,13,Num)
    Sect(Col1,1,16,Num)
    Sect(Col1,1,24,Num)#I
    Sect(Col2,2,25,Num)
    Sect(Col1,1,27,Num)
    Sect(Col1,1,30,Num)#P
    Sect(Col2,7,31,Num)
    Sect(Col1,1,38,Num)
    Num=2
    Sect(Col1,1,0,Num)#B
    Sect(Col2,2,1,Num)
    Sect(Col1,4,3,Num)
    Sect(Col2,2,7,Num)
    Sect(Col1,1,9,Num)
    Sect(Col1,1,12,Num)#L
    Sect(Col2,3,13,Num)
    Sect(Col1,1,16,Num)
    Sect(Col1,1,24,Num)#I
    Sect(Col2,2,25,Num)
    Sect(Col1,1,27,Num)
    Sect(Col1,1,30,Num)#P
    Sect(Col2,2,31,Num)
    Sect(Col1,4,33,Num)
    Sect(Col2,2,37,Num)
    Sect(Col1,1,39,Num)
    Num=3
    Sect(Col1,1,0,Num)#B
    Sect(Col2,2,1,Num)
    Sect(Col1,1,3,Num)
    Sect(Col1,1,7,Num)
    Sect(Col2,2,8,Num)
    Sect(Col1,1,9,Num)
    Sect(Col1,1,12,Num)#L
    Sect(Col2,3,13,Num)
    Sect(Col1,1,16,Num)
    Sect(Col1,1,24,Num)#I
    Sect(Col2,2,25,Num)
    Sect(Col1,1,27,Num)
    Sect(Col1,1,30,Num)#P
    Sect(Col2,2,31,Num)
    Sect(Col1,1,33,Num)
    Sect(Col1,1,37,Num)
    Sect(Col2,1,38,Num)
    Sect(Col1,1,39,Num)
    Num=4
    Sect(Col1,1,0,Num)#B
    Sect(Col2,2,1,Num)
    Sect(Col1,4,3,Num)
    Sect(Col2,2,7,Num)
    Sect(Col1,1,9,Num)
    Sect(Col1,1,12,Num)#L
    Sect(Col2,3,13,Num)
    Sect(Col1,1,16,Num)
    Sect(Col1,1,24,Num)#I
    Sect(Col2,2,25,Num)
    Sect(Col1,1,27,Num)
    Sect(Col1,1,30,Num)#P
    Sect(Col2,2,31,Num)
    Sect(Col1,1,33,Num)
    Sect(Col1,1,37,Num)
    Sect(Col2,1,38,Num)
    Sect(Col1,1,39,Num)
    Num=5
    Sect(Col1,1,0,Num)#B
    Sect(Col2,7,1,Num)
    Sect(Col1,1,8,Num)
    Sect(Col1,1,12,Num)#L
    Sect(Col2,3,13,Num)
    Sect(Col1,1,16,Num)
    Sect(Col1,1,24,Num)#I
    Sect(Col2,2,25,Num)
    Sect(Col1,1,27,Num)
    Sect(Col1,1,30,Num)#P
    Sect(Col2,2,31,Num)
    Sect(Col1,1,33,Num)
    Sect(Col1,1,37,Num)
    Sect(Col2,1,38,Num)
    Sect(Col1,1,39,Num)
    Num=6
    Sect(Col1,1,0,Num)#B
    Sect(Col3,2,1,Num)
    Sect(Col1,4,3,Num)
    Sect(Col3,2,7,Num)
    Sect(Col1,1,9,Num)
    Sect(Col1,1,12,Num)#L
    Sect(Col3,3,13,Num)
    Sect(Col1,1,16,Num)
    Sect(Col1,1,24,Num)#I
    Sect(Col3,2,25,Num)
    Sect(Col1,1,27,Num)
    Sect(Col1,1,30,Num)#P
    Sect(Col3,2,31,Num)
    Sect(Col1,4,33,Num)
    Sect(Col3,2,37,Num)
    Sect(Col1,1,39,Num)
    Num=7
    Sect(Col1,1,0,Num)#B
    Sect(Col3,2,1,Num)
    Sect(Col1,1,3,Num)
    Sect(Col1,1,7,Num)
    Sect(Col3,2,8,Num)
    Sect(Col1,1,9,Num)
    Sect(Col1,1,12,Num)#L
    Sect(Col3,3,13,Num)
    Sect(Col1,1,16,Num)
    Sect(Col1,1,24,Num)#I
    Sect(Col3,2,25,Num)
    Sect(Col1,1,27,Num)
    Sect(Col1,1,30,Num)#P
    Sect(Col3,7,31,Num)
    Sect(Col1,1,38,Num)
    Num=8
    Sect(Col1,1,0,Num)#B
    Sect(Col3,2,1,Num)
    Sect(Col1,1,3,Num)
    Sect(Col1,1,7,Num)
    Sect(Col3,2,8,Num)
    Sect(Col1,1,9,Num)
    Sect(Col1,1,12,Num)#L
    Sect(Col3,3,13,Num)
    Sect(Col1,1,16,Num)
    Sect(Col1,3,19,Num)
    Sect(Col1,1,24,Num)#I
    Sect(Col3,2,25,Num)
    Sect(Col1,1,27,Num)
    Sect(Col1,1,30,Num)#P
    Sect(Col3,2,31,Num)
    Sect(Col1,5,33,Num)
    Num=9
    Sect(Col1,1,0,Num)#B
    Sect(Col3,2,1,Num)
    Sect(Col1,4,3,Num)
    Sect(Col3,2,7,Num)
    Sect(Col1,1,9,Num)
    Sect(Col1,1,12,Num)#L
    Sect(Col3,3,13,Num)
    Sect(Col1,4,16,Num)
    Sect(Col3,1,20,Num)
    Sect(Col1,1,21,Num)
    Sect(Col1,1,24,Num)#I
    Sect(Col3,2,25,Num)
    Sect(Col1,1,27,Num)
    Sect(Col1,1,30,Num)#P
    Sect(Col3,2,31,Num)
    Sect(Col1,1,33,Num)
    Num=10
    Sect(Col1,1,0,Num)#B
    Sect(Col3,7,1,Num)
    Sect(Col1,1,8,Num)
    Sect(Col1,1,12,Num)#L
    Sect(Col3,8,13,Num)
    Sect(Col1,1,21,Num)
    Sect(Col1,1,24,Num)#I
    Sect(Col3,2,25,Num)
    Sect(Col1,1,27,Num)
    Sect(Col1,1,30,Num)#P
    Sect(Col3,2,31,Num)
    Sect(Col1,1,33,Num)
    Num=11
    Sect(Col1,8,0,Num)#B
    Sect(Col1,10,12,Num)#L
    Sect(Col1,4,24,Num)#I
    Sect(Col1,4,30,Num)#P


    # Written by Jack Smith
    
    
    # timy goto the top posoition
    timmy.goto(-160,160)
    timmy.left(90)
    if devop=='on':
        turtle.update()

    powerup=3
    gridval=grid_creation(Dif)
    dist=320/(Dif*10)

    p1pu=0
    p2pu=0
    choice1='s'

    range1=10*Dif
    
    timmy.goto(-160,-170)
    for i in range (0,range1):
        xval3=(-160+(dist*i)+(dist/2))
        timmy.goto(xval3,-170)
        if len(list(str(i))) == 1:
            ist=str(i)
            newi='0'+ist
        else:
            newi=i
        timmy.write(newi)

    
    timmy.goto(-170,-160)
    for i in range (0,range1):
        yval3=(-160+(dist*i)+(dist/2))
        timmy.goto(-170,yval3)
        if len(list(str(i))) == 1:
            i5=(10*Dif-(i+1))
            ist=str(i5)
            newi='0'+ist
        else:
            newi=i
        timmy.write(newi)

    run = True
    p1_powerup=0
    p2_powerup=0


    choice3='a'
    while choice3 != '':
        email_user=input('Please enter your email address: ')
        password=input('Please enter your password: ')
        for i in range (0,1000):
            print('')
        sendto=input('Please enter the recieving email: ')
        player_number=int(input('Please enter if you are player 1 or 2: '))
        emailcount=0
        while True:
            if player_number == 1:
                oplayer_colour=Col3
            elif player_number == 2:
                oplayer_colour=Col2
            if player_number == 1 :
                choice3=coordinates_input(1,p1_powerup,Col2,dist)
                recieve_mail()
            elif player_number == 2:
                recieve_mail()
                choice3=coordinates_input(2,p2_powerup,Col3,dist)
        
