import turtle
import math
import random
from random import randint

T=turtle
T.speed(100)
T.penup()
T.goto(-160,272)

def Score(X,Y,Z):
    T.penup()
    T.color(X)
    T.goto(-160+(320/((Dif)*10))*Y,160-(320/(Dif*10))*Z)
    if Y>=1 and Y<=Dif*10 and Z>=1 and Z<=Dif*10:
        T.begin_fill()
        for i in range(0,4):
            T.forward(320/(Dif*10))
            T.lt(90)
        T.end_fill()
    
def Box(X,Y,Z):
    T.color(X)
    T.goto(Y,Z)
    T.begin_fill()
    for i in range(0,4):
        T.forward(8)
        T.lt(90)
    T.end_fill()

def ForLoop():
    Tru=Tru+1
    return Tru

def Sect(Col,Amount,x,y):
    if Amount==1:
        Box(Col,-160+8*x,272-8*y)
    else:
        for i in range (0,Amount):
            Box(Col,-160+8*(x+i),272-8*y)

#The difficulty and grid setup system
            
Dif=int(input("Difficulty Level (1-4): "))
turtle.tracer(0)
T.goto(-160,-160)
T.color("Grey")
T.pendown()
for a in range (0,Dif*10+2):
    T.forward (320)
    T.penup()
    T.goto(-160,-160+320/(Dif*10)*a)
    T.pendown()
T.rt(90)
T.penup()
for a in range (0,Dif*10+2):
    T.forward (320)
    T.penup()
    T.goto(-160+320/(Dif*10)*a,160)
    T.pendown()
T.penup()
T.goto(-160,272)
colors  = ["red","green","blue","orange","purple","pink","yellow"]
Col1="Black"
Col2=random.choice(colors)
Col3=random.choice(colors)

#The logo for the game in the startup screen

Num=0
Sect(Col1,8,0,Num)#B
Sect(Col1,5,12,Num)#L
Sect(Col1,4,24,Num)#I
Sect(Col1,8,30,Num)#P
Num=1
Sect(Col1,1,0,Num)#B
Sect(Col2,7,1,Num)
Sect(Col1,1,8,Num)
Sect(Col1,1,12,Num)#L
Sect(Col2,3,13,Num)
Sect(Col1,1,16,Num)
Sect(Col1,1,24,Num)#I
Sect(Col2,2,25,Num)
Sect(Col1,1,27,Num)
Sect(Col1,1,30,Num)#P
Sect(Col2,7,31,Num)
Sect(Col1,1,38,Num)
Num=2
Sect(Col1,1,0,Num)#B
Sect(Col2,2,1,Num)
Sect(Col1,4,3,Num)
Sect(Col2,2,7,Num)
Sect(Col1,1,9,Num)
Sect(Col1,1,12,Num)#L
Sect(Col2,3,13,Num)
Sect(Col1,1,16,Num)
Sect(Col1,1,24,Num)#I
Sect(Col2,2,25,Num)
Sect(Col1,1,27,Num)
Sect(Col1,1,30,Num)#P
Sect(Col2,2,31,Num)
Sect(Col1,4,33,Num)
Sect(Col2,2,37,Num)
Sect(Col1,1,39,Num)
Num=3
Sect(Col1,1,0,Num)#B
Sect(Col2,2,1,Num)
Sect(Col1,1,3,Num)
Sect(Col1,1,7,Num)
Sect(Col2,2,8,Num)
Sect(Col1,1,9,Num)
Sect(Col1,1,12,Num)#L
Sect(Col2,3,13,Num)
Sect(Col1,1,16,Num)
Sect(Col1,1,24,Num)#I
Sect(Col2,2,25,Num)
Sect(Col1,1,27,Num)
Sect(Col1,1,30,Num)#P
Sect(Col2,2,31,Num)
Sect(Col1,1,33,Num)
Sect(Col1,1,37,Num)
Sect(Col2,1,38,Num)
Sect(Col1,1,39,Num)
Num=4
Sect(Col1,1,0,Num)#B
Sect(Col2,2,1,Num)
Sect(Col1,4,3,Num)
Sect(Col2,2,7,Num)
Sect(Col1,1,9,Num)
Sect(Col1,1,12,Num)#L
Sect(Col2,3,13,Num)
Sect(Col1,1,16,Num)
Sect(Col1,1,24,Num)#I
Sect(Col2,2,25,Num)
Sect(Col1,1,27,Num)
Sect(Col1,1,30,Num)#P
Sect(Col2,2,31,Num)
Sect(Col1,1,33,Num)
Sect(Col1,1,37,Num)
Sect(Col2,1,38,Num)
Sect(Col1,1,39,Num)
Num=5
Sect(Col1,1,0,Num)#B
Sect(Col2,7,1,Num)
Sect(Col1,1,8,Num)
Sect(Col1,1,12,Num)#L
Sect(Col2,3,13,Num)
Sect(Col1,1,16,Num)
Sect(Col1,1,24,Num)#I
Sect(Col2,2,25,Num)
Sect(Col1,1,27,Num)
Sect(Col1,1,30,Num)#P
Sect(Col2,2,31,Num)
Sect(Col1,1,33,Num)
Sect(Col1,1,37,Num)
Sect(Col2,1,38,Num)
Sect(Col1,1,39,Num)
Num=6
Sect(Col1,1,0,Num)#B
Sect(Col3,2,1,Num)
Sect(Col1,4,3,Num)
Sect(Col3,2,7,Num)
Sect(Col1,1,9,Num)
Sect(Col1,1,12,Num)#L
Sect(Col3,3,13,Num)
Sect(Col1,1,16,Num)
Sect(Col1,1,24,Num)#I
Sect(Col3,2,25,Num)
Sect(Col1,1,27,Num)
Sect(Col1,1,30,Num)#P
Sect(Col3,2,31,Num)
Sect(Col1,4,33,Num)
Sect(Col3,2,37,Num)
Sect(Col1,1,39,Num)
Num=7
Sect(Col1,1,0,Num)#B
Sect(Col3,2,1,Num)
Sect(Col1,1,3,Num)
Sect(Col1,1,7,Num)
Sect(Col3,2,8,Num)
Sect(Col1,1,9,Num)
Sect(Col1,1,12,Num)#L
Sect(Col3,3,13,Num)
Sect(Col1,1,16,Num)
Sect(Col1,1,24,Num)#I
Sect(Col3,2,25,Num)
Sect(Col1,1,27,Num)
Sect(Col1,1,30,Num)#P
Sect(Col3,7,31,Num)
Sect(Col1,1,38,Num)
Num=8
Sect(Col1,1,0,Num)#B
Sect(Col3,2,1,Num)
Sect(Col1,1,3,Num)
Sect(Col1,1,7,Num)
Sect(Col3,2,8,Num)
Sect(Col1,1,9,Num)
Sect(Col1,1,12,Num)#L
Sect(Col3,3,13,Num)
Sect(Col1,1,16,Num)
Sect(Col1,3,19,Num)
Sect(Col1,1,24,Num)#I
Sect(Col3,2,25,Num)
Sect(Col1,1,27,Num)
Sect(Col1,1,30,Num)#P
Sect(Col3,2,31,Num)
Sect(Col1,5,33,Num)
Num=9
Sect(Col1,1,0,Num)#B
Sect(Col3,2,1,Num)
Sect(Col1,4,3,Num)
Sect(Col3,2,7,Num)
Sect(Col1,1,9,Num)
Sect(Col1,1,12,Num)#L
Sect(Col3,3,13,Num)
Sect(Col1,4,16,Num)
Sect(Col3,1,20,Num)
Sect(Col1,1,21,Num)
Sect(Col1,1,24,Num)#I
Sect(Col3,2,25,Num)
Sect(Col1,1,27,Num)
Sect(Col1,1,30,Num)#P
Sect(Col3,2,31,Num)
Sect(Col1,1,33,Num)
Num=10
Sect(Col1,1,0,Num)#B
Sect(Col3,7,1,Num)
Sect(Col1,1,8,Num)
Sect(Col1,1,12,Num)#L
Sect(Col3,8,13,Num)
Sect(Col1,1,21,Num)
Sect(Col1,1,24,Num)#I
Sect(Col3,2,25,Num)
Sect(Col1,1,27,Num)
Sect(Col1,1,30,Num)#P
Sect(Col3,2,31,Num)
Sect(Col1,1,33,Num)
Num=11
Sect(Col1,8,0,Num)#B
Sect(Col1,10,12,Num)#L
Sect(Col1,4,24,Num)#I
Sect(Col1,4,30,Num)#P

turtle.update()
    
#Colour arrangement and everything to do with that stuff

T.lt(180)
for i in range (0,999):
    X=randint(1,Dif*10)
    Y=randint(1,Dif*10)
    Score(Col2,X,Y)
    X=randint(1,Dif*10)
    Y=randint(1,Dif*10)
    Score(Col3,X,Y)




























